package com.example.fyp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LogIn extends AppCompatActivity {
    EditText u_email, u_password;
    Button loginbtn,forgotbtn;
    FirebaseAuth fAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        u_email =findViewById(R.id.txt_email);
        u_password =findViewById(R.id.txt_password);
        fAuth = FirebaseAuth.getInstance();
        loginbtn = findViewById(R.id.b_login);
        forgotbtn=findViewById(R.id.bt_forgot);

        loginbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = u_email.getText().toString().trim();
                final  String pass = u_password.getText().toString().trim();

                if (TextUtils.isEmpty(email)) {
                    u_email.setError("Required Field");
                    return;
                }
                if (TextUtils.isEmpty(pass)) {
                    u_password.setError("Required Field");
                    return;
                }
                fAuth.signInWithEmailAndPassword(email,pass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @SuppressLint("ResourceType")
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()){
                            if (fAuth.getCurrentUser().isEmailVerified()){
                                Toast.makeText(LogIn.this, "User Logged In Succesfuly", Toast.LENGTH_SHORT).show();
                                setContentView(R.layout.activity_main);
                                Intent intent = new Intent(getApplicationContext(),Interface.class);
                                startActivity(intent);
                            }else{ Toast.makeText(LogIn.this, "Please Verify Your Email!" , Toast.LENGTH_SHORT).show();

                            }
                        } else {
                            Toast.makeText(LogIn.this, "Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });
        forgotbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = u_email.getText().toString().trim();
                FirebaseAuth.getInstance().sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if(task.isSuccessful()){
                            setContentView(R.layout.activity_log_in);
                            startActivity(new Intent(getApplicationContext(), LogIn.class));
                            Toast.makeText(LogIn.this, "Please check your email and reset your password", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }
}
