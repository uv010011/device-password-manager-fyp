package com.example.fyp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Registration extends AppCompatActivity {
    EditText Email, password;
    Button registerbtn;
    ProgressBar progressBar;
     FirebaseAuth fAuth;
     Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");
    List<String> check;
     @Override
    protected void onStart() {
         super.onStart();
         setContentView(R.layout.activity_registration);
         Email = findViewById(R.id.Email);
         password = findViewById(R.id.password);
         registerbtn = findViewById(R.id.bt_reg);
         fAuth = FirebaseAuth.getInstance();

         registerbtn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 final String email = Email.getText().toString().trim();
                final  String pass = password.getText().toString().trim();

                 if (TextUtils.isEmpty(email)) {
                     Email.setError("Required Field");
                     return;
                 }
                 if (TextUtils.isEmpty(pass)) {
                     password.setError("Required Field");
                     return;
                 }
                 Matcher matcher = pattern.matcher(pass);
                 if (pass.length() < 6 || matcher.matches()) {
                     password.setError("Add 6 or more characters including numeric and special characters");
                     return;
                 }
                 final String finalPass = pass;
                 checkEmail(email, new EmailCheckListener() {
                     @Override
                     public void onSuccess(boolean isReg) {
                         if (isReg) {
                             setContentView(R.layout.activity_log_in);
                             startActivity(new Intent(getApplicationContext(), LogIn.class));
                             Toast.makeText(Registration.this, "Email is Already In Use Try Logging In", Toast.LENGTH_SHORT).show();
                         } else {
                             fAuth.createUserWithEmailAndPassword(email, finalPass).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                 @Override
                                 public void onComplete(@NonNull Task<AuthResult> task) {
                                     if (task.isSuccessful()) {
                                        fAuth.getCurrentUser().sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                if (task.isSuccessful()) {
                                                    Toast.makeText(Registration.this, "Account Created Succesfuly.Please check your email and verify your account.", Toast.LENGTH_SHORT).show();
                                                }
                                            }});
                                         setContentView(R.layout.activity_main);
                                         Intent intent = new Intent(getApplicationContext(),Interface.class);
                                         startActivity(intent);
                                     } else {
                                         Toast.makeText(Registration.this, "Error" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                                     }
                                 }
                             });
                         }
                     }
                 });

             }
         });
     }
public interface EmailCheckListener{
         void onSuccess(boolean isReg);
    }
    public void checkEmail(final String email, final EmailCheckListener listener){
         fAuth.fetchSignInMethodsForEmail(email).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
             @Override
             public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                 boolean check = !task.getResult().getSignInMethods().isEmpty();
                 listener.onSuccess(check);
             }
         });
         }
}

