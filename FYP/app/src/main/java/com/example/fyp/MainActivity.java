package com.example.fyp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Main Activity implements an OnClickListener switch case for the two buttons
 * 1st is for when the user selects to register a new account
 * 2nd is for when a returning user logs in
 * In both cases the corresponding activity is loaded
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button registration,login; //declare the buttons

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start); //loads the starting screen with the two buttons
       //initialisation of the buttons and the ClickListneners
        registration = findViewById(R.id.bt_mmreg);
        login = findViewById(R.id.bt_mmlogin);
        registration.setOnClickListener(this);
        login.setOnClickListener(this);
    }

    /**
     * onClick it gets the ID of the button and starts the appropriate activity
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.bt_mmreg: // case for the registration
                startActivity(new Intent(getApplicationContext(), Registration.class));
                break;
            case R.id.bt_mmlogin://case for the login of a returning customer
                startActivity(new Intent(getApplicationContext(), LogIn.class));
                break;
        }
    }
}